package com.example.giuseppe.hackernewsv2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PopularFragment extends android.support.v4.app.Fragment implements rvAdapterNews.ItemClickListener {

    public ProgressBar pbPopular;
    public RecyclerView rvNews;
    public rvAdapterNews adapter;
    public String[] urlOrAuthor = {"Visita la notizia", "Vedi l'autore"};

    public String title, link, by;
    public String listCodeSubmitted;

    public String karma, authorName;
    public Integer submitted;

    public View rootView;

    public List<String> listIdNews = new ArrayList<String>();
    public ArrayList<DetailNews> dN = new ArrayList<>();

    public PopularFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_popular, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("News più popolari");


        pbPopular = rootView.findViewById(R.id.pbPopular);
        rvNews = rootView.findViewById(R.id.rvPopularFragment);
        rvNews.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));

        if(!isInternetAvailable()){
            Snackbar snackbar = Snackbar
                    .make(getView(), "Connessione internet assente.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }else {
            new PopularFragment.FirstTask().execute("https://hacker-news.firebaseio.com/v0/topstories.json?&orderBy=%22$key%22&limitToFirst=15");
        }
    }

    public boolean isInternetAvailable() {
        try {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {

            Log.e("isInternetAvailable:",e.toString());
            return false;
        }
    }

    private class FirstTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pbPopular.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line);

                    String str[] = line.split(",");
                    str[0].replace("[","");
                    str[str.length-1].replace("]","");

                    listIdNews = Arrays.asList(str);

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            retriveNews();
        }


    }

    private class JsonTask2 extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pbPopular.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }

                String finalJson = buffer.toString();

                JSONObject finalObject = new JSONObject(finalJson);

                by = finalObject.getString("by");
                title = finalObject.getString("title");
                link = finalObject.getString("url");

                dN.add(new DetailNews(by,title,link));

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println(result);

            try{
                adapter = new rvAdapterNews(getActivity().getApplicationContext(), dN);
            }catch (Exception e){
                e.printStackTrace();
            }

            rvNews.setAdapter(adapter);

            if (pbPopular.isShown()){
                pbPopular.setVisibility(View.INVISIBLE);
            }

            selectItemClicked();
            scrollListener();

        }
    }

    private class AuthorTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
        }

        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }

                String finalJson = buffer.toString();

                JSONObject finalObject = new JSONObject(finalJson);

                karma = finalObject.getString("karma");
                listCodeSubmitted = finalObject.getString("submitted");


                String str[] = listCodeSubmitted.split(",");
                submitted = str.length;
                System.out.println(submitted);


                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            AlertDialog.Builder builderAuthor = new AlertDialog.Builder(getContext());
            builderAuthor.setTitle("AUTORE");
            String[] strToShow = {authorName, "Rating: " + karma, "Numero di articoli: " + submitted.toString()};
            builderAuthor.setItems(strToShow, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterfaceAuthor, int which) {
                    if (which == 0) {
                        System.out.println("");
                    }
                }
            });
            builderAuthor.show();


        }
    }

    public void retriveNews(){
        for (int i = 0; i <15; i++) {
            new PopularFragment.JsonTask2().execute("https://hacker-news.firebaseio.com/v0/item/" + listIdNews.get(i) + ".json");
        }
    }

    public void retriveAuthor(Integer pos){
        authorName = dN.get(pos).by;
        new PopularFragment.AuthorTask().execute("https://hacker-news.firebaseio.com/v0/user/" + dN.get(pos).by + ".json" );
    }

    @Override
    public void onItemClick(View view, int position) {
        final int pos = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Cosa vuoi consultare?");
        builder.setItems(urlOrAuthor, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface mdialog, int which) {
                if (which == 0){
                    String url = dN.get(pos).url;

                    WebView wv = new WebView(getContext());
                    wv.loadUrl(url);
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    wv.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });
                    alert.setView(wv);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = alert.create();
                    dialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER;
                    dialog.getWindow().setAttributes(lp);
                    Button positiveButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                    LinearLayout parent = (LinearLayout) positiveButton.getParent();
                    parent.setGravity(Gravity.CENTER_HORIZONTAL);
                    View leftSpacer = parent.getChildAt(1);
                    leftSpacer.setVisibility(View.GONE);
                }else{
                    System.out.println("id news: " + dN.get(pos));
                    System.out.println("https://hacker-news.firebaseio.com/v0/user/" + dN.get(pos).by + ".json");

                    retriveAuthor(pos);
                }
            }
        });
        builder.show();

    }

    public void selectItemClicked(){
        adapter.setClickListener(this);
        rvNews.setAdapter(adapter);
    }

    public void scrollListener(){
        rvNews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    Snackbar snackbar = Snackbar
                            .make(getView(), "End of news", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });
    }


}
