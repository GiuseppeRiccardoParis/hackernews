package com.example.giuseppe.hackernewsv2;

public class DetailNews {

    public String by;
    public String title;
    //public String type;
    public String url;

    DetailNews(String by, String title, /*String type,*/ String url) {
        this.by = by;
        this.title = title;
        //this.type = type;
        this.url = url;
    }

}
