package com.example.giuseppe.hackernewsv2;

public class DetailAuthor {

    public String id;
    public String karma;
    public String[] submitted;

    DetailAuthor(String id, String karma, String[] submitted) {
        this.id = id;
        this.karma = karma;
        this.submitted = submitted;
    }

}
