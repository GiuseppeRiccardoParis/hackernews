package com.example.giuseppe.hackernewsv2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HNClient {

    //ID Ultime NEWS
    @GET("newstories.json")
    Call<List<Integer>> getIdNewStories();

    //ID NEWS più popolari
    @GET("topstories.json")
    Call<List<Integer>> getIdTopStories();

    //Dettagli news
    @GET("item/{idnews}.json")
    Call<DetailNews> getDetailNews(@Path("idnews") int idn);

    //Dettagli autori
    @GET("user/{author}.json")
    Call<DetailAuthor> getInfoAuthor(@Path("author") String aut);

}
