package com.example.giuseppe.hackernewsv2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends android.support.v4.app.Fragment implements rvAdapterNews.ItemClickListener {

    private ProgressBar pbHome;
    private RecyclerView rvNews;
    private rvAdapterNews adapter;
    private String[] urlOrAuthor = {"Visita la notizia", "Vedi l'autore"};

    public View rootView;

    private Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl("https://hacker-news.firebaseio.com/v0/")
            .addConverterFactory(GsonConverterFactory.create());
    private Retrofit retrofit;
    private HNClient client;

    private ArrayList<DetailNews> dN = new ArrayList<>();
    private List<Integer> idNewStories;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Ultime News");

        pbHome = rootView.findViewById(R.id.pbHome);
        rvNews = rootView.findViewById(R.id.rvHomeFragment);
        rvNews.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));

        if(!isInternetAvailable()){
            Snackbar snackbar = Snackbar
                    .make(getView(), "Connessione internet assente.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }else{

            retrofit = builder.build();
            client = retrofit.create(HNClient.class);

            //Asynchronous CallBack [IDs New Stories - Detail of Each News]
            client.getIdNewStories().enqueue(new Callback<List<Integer>>() {
                @Override
                public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                    idNewStories = response.body();

                    for (int i = 0; i < 15; i++) {
                        client.getDetailNews(idNewStories.get(i)).enqueue(new Callback<DetailNews>() {
                            @Override
                            public void onResponse(Call<DetailNews> call, Response<DetailNews> response) {
                                try {
                                    String by = response.body().by;
                                    String title = response.body().title;
                                    String link = response.body().url;

                                    dN.add(new DetailNews(by,title,link));
                                    adapter = new rvAdapterNews(getActivity().getApplicationContext(), dN);

                                    rvNews.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();

                                    selectItemClicked();
                                    scrollListener();

                                }catch(Exception e){}
                            }

                            @Override
                            public void onFailure(Call<DetailNews> call, Throwable t) {
                                Toast.makeText(getContext(), "Errore", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<List<Integer>> call, Throwable t) {
                    Toast.makeText(getContext(), "Errore", Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    public boolean isInternetAvailable() {
        try {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {

            Log.e("isInternetAvailable:",e.toString());
            return false;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        final int pos = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Cosa vuoi consultare?");
        builder.setItems(urlOrAuthor, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface mdialog, int which) {
                if (which == 0){
                    String url = dN.get(pos).url;
                    System.out.println("URL_: " + dN.get(pos).url);

                    WebView wv = new WebView(getContext());
                    wv.loadUrl(url);
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    wv.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });
                    alert.setView(wv);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = alert.create();
                    dialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER;
                    dialog.getWindow().setAttributes(lp);
                    Button positiveButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                    LinearLayout parent = (LinearLayout) positiveButton.getParent();
                    parent.setGravity(Gravity.CENTER_HORIZONTAL);
                    View leftSpacer = parent.getChildAt(1);
                    leftSpacer.setVisibility(View.GONE);
                }else{
                    authorInfo(pos);
                }
            }
        });
        builder.show();

    }

    public void authorInfo(int pos){

        //Asynchronous CallBack
        client.getInfoAuthor(dN.get(pos).by).enqueue(new Callback<DetailAuthor>() {
            @Override
            public void onResponse(Call<DetailAuthor> call, Response<DetailAuthor> response) {
                try {
                    String idAuthor = response.body().id;

                    String karmaAuthor = response.body().karma;
                    String[] submitted = response.body().submitted;

                    AlertDialog.Builder builderAuthor2 = new AlertDialog.Builder(getContext());
                    builderAuthor2.setTitle("AUTORE");
                    String[] strToShow2 = {idAuthor, "Karma: " + karmaAuthor, "Submitted: " + submitted.length};
                    builderAuthor2.setItems(strToShow2, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterfaceAuthor, int which) {

                        }
                    });
                    builderAuthor2.show();
                }catch(Exception e){}
            }

            @Override
            public void onFailure(Call<DetailAuthor> call, Throwable t) {
                Toast.makeText(getContext(), "Errore", Toast.LENGTH_LONG).show();
            }

        });
    }


    public void selectItemClicked(){
        adapter.setClickListener(this);
        rvNews.setAdapter(adapter);
    }

    public void scrollListener(){
        rvNews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    Snackbar snackbar = Snackbar
                            .make(getView(), "End of news", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });
    }

}
